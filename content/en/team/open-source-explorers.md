---
title: Explorers
description: Grey Software's Open Source Explorers
category: Team
postion: 3
explorers:  
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
---

## Explorers

<team-profiles :profiles="explorers"></team-profiles>
