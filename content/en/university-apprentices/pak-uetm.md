---
title: PAK - UET Mardan
description: 
position: 07
category: University Apprentices
spr2021:
  mentors:
    - name: Arsala
      avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
      github: https://github.com/ArsalaBangash
      gitlab: https://gitlab.com/ArsalaBangash
      linkedin: https://linkedin.com/in/ArsalaBangash

    - name: Adil Shehzad
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7507821/avatar.png
      github: adilshehzad786
      gitlab: adilshehzad
      linkedin: adilshehzad7

  apprentices:
    - name: Faraz Ahmad Khan
      avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/7891204/avatar.png
      github: https://github.com/farazahmadkhan15
      gitlab: https://gitlab.com/farazahmadkhan15
      linkedin: https://www.linkedin.com/in/farazahmadkhan15/


---

## Spring 2021 Cohort

### Mentors

<team-profiles :profiles="spr2021.mentors"></team-profiles>

### Apprentices

<team-profiles :profiles="spr2021.apprentices"></team-profiles>
